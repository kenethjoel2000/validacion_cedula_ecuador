const express = require('express');

const app = express();
app.use(express.json());
app.use(express.urlencoded());

function salida(JSON) {
    var today = new Date();
    var date = today.getFullYear() + '-' + today.getMonth() + '-' + today.getDay();

    if (JSON.Codigo == '200') return {
        Cedula: JSON.Cedula,
        Tipo:  JSON.Datos.Tipo,
        mensaje : JSON.Mensaje,
        fecha : date,
        resultado : JSON.Codigo
    }

    if (JSON.Codigo) return {
        mensaje : JSON.Mensaje,
        fecha : date,
        resultado : JSON.Codigo
    }
}


app.get('/validar',function(req,res) {
    try{
        var x = Validar(req.body.Cedula);
        if (x.Codigo == 200) {
            return res.status(200).json(salida(x));
        } else if (x.Codigo == 500) {
            return res.status(500).json(salida(x));
        }
    }catch(e){return{
            Mensaje : e,
            Codigo : 500
        }    
    }
});

function Validar(c){
    try{
        var Datos = ObtenerDatos(c);
        if (13 >= Datos.Cedula.length && Datos.Cedula.length >= 10) {
            if( Datos.c12 >= 1 && Datos.c12 <=24){
                Verificar_N10(Datos);
                LlenarMensaje(Datos);
            }else{
                Datos.Mensaje = "Cedula No pertence a un Provincia del Ecuador"; Datos.Codigo = 500;
            }
        }else{
            Datos.Mensaje = "Datos de Cedula o Ruc de [10 digitos a 13 Digitos"; Datos.Codigo = 500;
        }
        return Datos;
    }catch(e){
        Datos.Mensaje = e; Datos.Codigo = 500;
        return Datos;
    }

}

function LlenarMensaje(Datos) {
    if (Datos.c3 == 6) {
        if (Datos.Numero_V == Datos.c9) {
            Datos.Mensaje = "Cedula Verificada";Datos.Codigo = 200;
        } else {
            Datos.Mensaje = "Cedula No Verificada";Datos.Codigo = 500;
        }
    } else {
        if (Datos.Numero_V == Datos.c10) {
            Datos.Mensaje = "Cedula Verificada";Datos.Codigo = 200;
        } else {
            Datos.Mensaje = "Cedula No Verificada";Datos.Codigo = 500;
        }
    }
}

function Verificar_N10(Datos){
    var Modulo = Datos.Datos.Modulo;
    var SumaTotal = Sumar_0N9(Datos);
    if (Math.trunc(SumaTotal % Modulo) == 0) {
        var Digito_V = 0;
        Datos.Numero_V = Digito_V;
        return Digito_V;
    } else {
        var Digito_V = Modulo - (Math.trunc(SumaTotal % Modulo));
        Datos.Numero_V = Digito_V;
        return Digito_V;
    }
}

function Sumar_0N9(Datos){
    var sumatoria=0;
    Datos.Datos.Cadena.forEach(function (valor, indice) {
        var x = valor * Datos.c0_9A[indice];
        if (x >= 10 && Datos.Datos.Modulo == 10) {
            sumatoria = sumatoria + (SumerDigitos(x));
        } else {
            sumatoria = sumatoria + (x);
        }
    });
    return sumatoria;
}

function SumerDigitos(x){
    var aux = 0;
    while (x > 0) {
        aux += x % 10;
        x = x / 10;
    }
    return aux;
}


function Verificar_N3(c3){
    if (1 <= c3 && c3 <= 5) {
        return {Tipo: "R.U.C. Natural", Cadena: [2,1,2,1,2,1,2,1,2] , Modulo:10};
    } else if (c3 == 6){
        return {Tipo: "R.U.C. Públicos:", Cadena: [3,2,7,6,5,4,3,2], Modulo:11};
    } else if (c3 == 9){
        return {Tipo: "R.U.C. Jurídicos y extranjeros sin cédula", Cadena: [4,3,2,7,6,5,4,3,2], Modulo:11};
    }
}

function ObtenerDatos(c){
    return {
        Cedula : c,
        c12 : c.substring(0,2),
        c3 : c.substring(2,3),
        c9 : c.substring(8,9),
        c10 : c.substring(9,10),
        c0_9A : Array.from(c.substring(0,9)),
        Datos : Verificar_N3(c.substring(2,3)),
        Numero_V: null,
        Mensaje: "",
        Codigo: 0
    }
}

app.listen(8001, () => {
    console.log('El servidor está inicializado en el puerto 8001')
});

exports.main ={
    Obtener:ObtenerDatos,
    
    V:Validar,
    V_N10: Verificar_N10,
    V_N3: Verificar_N3
};